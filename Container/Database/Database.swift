//
//  Database.swift
//  Container
//
//  Created by Sergey Melkonyan on 03.10.22.
//

import Foundation
import UIKit
import RealmSwift

let answerRealm = try! Realm()

class Database {
    static func saveInDatabase(answer: Answer) {
        let realmAnswer = answer.toObject()
        realmAnswer.answer = answer.answer
        answerRealm.beginWrite()
        answerRealm.add(realmAnswer, update: .all)
        try! answerRealm.commitWrite()
    }
    
    static func deleteFromDatabase() {
        answerRealm.beginWrite()
        answerRealm.delete(answerRealm.objects(RealmAnswer.self))
        try! answerRealm.commitWrite()
    }
}
