//
//  QuizViewControllerDelegate.swift
//  Container
//
//  Created by Sergey Melkonyan on 29.09.22.
//

import Foundation

protocol QuizViewControllerDelegate: AnyObject {
    func goBack()
}
