//
//  QuizTableViewCellDelegate.swift
//  Container
//
//  Created by Sergey Melkonyan on 29.09.22.
//

import Foundation
import UIKit

protocol QuizTableViewCellDelegate: AnyObject {
    func didTapButton(_ tableViewCell: QuizTableViewCell,button: UIButton)
}
