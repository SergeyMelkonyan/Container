//
//  ViewControllerDelegate.swift
//  Container
//
//  Created by Sergey Melkonyan on 29.09.22.
//

import Foundation

protocol ViewControllerDelegate: AnyObject {
    func nextButtonIsTapped()
}
