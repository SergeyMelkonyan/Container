//
//  QuizTableViewCell.swift
//  Container
//
//  Created by Sergey Melkonyan on 28.09.22.
//

import UIKit

class QuizTableViewCell: UITableViewCell {
    
    // MARK: - Subviews

    @IBOutlet weak var button: UIButton!
    
    // MARK: - Delegation
    
    weak var delegate: QuizTableViewCellDelegate?

    // MARK: - Nib and Identifier
    
    static let identifier = "QuizTableViewCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "QuizTableViewCell", bundle: nil)
    }
    
   static var previousButton: UIButton?
    // MARK: - Cell Configuration
    
    private var answer = Answer(answer: "")
    
    func configure(answer: Answer) {
        self.answer = answer
        if answer.isSelected == true {
            button.backgroundColor = .systemBlue
            button.setTitleColor(.white, for: .normal)
        } else {
            button.backgroundColor = UIColor(named: "cellButtonColor")
            button.setTitleColor(UIColor(red: 0.282, green: 0.282, blue: 0.29, alpha: 1), for: .normal)
        }
        
        button.setTitle(self.answer.answer, for: .normal)
    }
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        button.layer.cornerRadius = 5
        button.setTitleColor(UIColor(red: 0.282, green: 0.282, blue: 0.29, alpha: 1), for: .normal)
        button.configuration = .gray()
    }
    
    // MARK: - Callbacks
    
    @IBAction func answerButtonTapped(_ sender: UIButton) {
        delegate?.didTapButton(self, button: sender)
    }
}
