//
//  ViewController.swift
//  Container
//
//  Created by Sergey Melkonyan on 27.09.22.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {
    
    // MARK: - Subviews
    
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet private var nextButton: UIButton!
    
    // MARK: - Delegate
    
    weak static var delegate: ViewControllerDelegate?
    
    static var progress = UIProgressView()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(Realm.Configuration.defaultConfiguration.fileURL as Any)
        
        view.backgroundColor = UIColor(named: "viewControllerColor")
        progressBar.progress = 100 / Float(QuizPageModel.quizModel.count * 100)
        nextButton.layer.cornerRadius = 10
        nextButton.backgroundColor = UIColor(named: "buttonColor")
        
        QuizViewController.delegate = self
    }
    
    // MARK: - Callbacks
    
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        UIView.animate(withDuration: 0.7) {
            self.progressBar.setProgress(self.progressBar.progress + (100 / Float(QuizPageModel.quizModel.count * 100)), animated: true)
        }
        
        ViewController.delegate?.nextButtonIsTapped()
    }
}

extension ViewController: QuizViewControllerDelegate {
    
    // MARK: - Delegation
    
    func goBack() {
        UIView.animate(withDuration: 0.7) {
            self.progressBar.setProgress(self.progressBar.progress - 100 / Float(QuizPageModel.quizModel.count * 100), animated: true)
        }
    }
}
