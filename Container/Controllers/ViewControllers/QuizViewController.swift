//
//  TestViewController.swift
//  Container
//
//  Created by Sergey Melkonyan on 27.09.22.
//

import UIKit
class QuizViewController: UIViewController, UIGestureRecognizerDelegate {
    
    // MARK: - Subviews
    
    @IBOutlet weak var quizTableView: UITableView!
    
    @IBOutlet private var question: UILabel!
    @IBOutlet private var numberOfQuestion: UILabel!
    @IBOutlet private var image: UIImageView!
    
    // MARK: - NameSpaces
    
    enum QuestionIndex {
       static var questionIndex = -1
    }
    
    enum ChosenAnswers {
        static var chosenAnswers = [Answer]()
    }
    
    enum Controller {
        static var nextPageVC: QuizViewController = QuizViewController()
    }
    
    // MARK: - Static Properties
    
   static var previousButton: UIButton?
    
    var count = 0
    
    // MARK: - Delegate
    weak static var delegate: QuizViewControllerDelegate?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        quizTableView.register(QuizTableViewCell.nib(), forCellReuseIdentifier: QuizTableViewCell.identifier)
        
        self.quizTableView.delegate = self
        self.quizTableView.dataSource = self
        
        ViewController.delegate = self
        
        Controller.nextPageVC = storyboard?.instantiateViewController(withIdentifier: "QuizViewController") as! QuizViewController
        
        QuestionIndex.questionIndex += 1
        self.quizTableView.reloadData()
        
        Database.deleteFromDatabase()
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        if parent == nil {
            QuizViewController.delegate?.goBack()
            QuestionIndex.questionIndex -= 1
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        ViewController.delegate = self
        super.viewWillAppear(animated)
    }
}

extension QuizViewController: UITableViewDelegate,
                              UITableViewDataSource,
                              ViewControllerDelegate,
                              QuizTableViewCellDelegate {
    
    // MARK: - TableView Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        QuizPageModel.quizModel[QuestionIndex.questionIndex].answers.count
    }
    
    // MARK: - TableView Datasource
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = quizTableView.dequeueReusableCell(withIdentifier: "QuizTableViewCell", for: indexPath) as! QuizTableViewCell
        setStaticFieldsValues()
        
        cell.configure(answer: QuizPageModel.quizModel[QuestionIndex.questionIndex].answers[indexPath.row])
        
        cell.delegate = self
        return cell
    }
    
    // MARK: - Helpers
    
    func setStaticFieldsValues() {
        question.text = QuizPageModel.quizModel[QuestionIndex.questionIndex].question
        numberOfQuestion.text = "question \(QuestionIndex.questionIndex + 1) of \(QuizPageModel.quizModel.count)"
        image.image = UIImage(named: QuizPageModel.quizModel[QuestionIndex.questionIndex].image)
    }
    
    // MARK: - Delegation
    
    func nextButtonIsTapped() {
        if QuestionIndex.questionIndex == QuizPageModel.quizModel.count - 1 {
            let vc = UIViewController()
            vc.view.backgroundColor = .yellow
            self.navigationController?.pushViewController(vc, animated: true)
            
            let answers = QuizPageModel.quizModel
                .map { $0.answers
                .filter { $0.isSelected == true }}
                .flatMap { $0 }
            for i in answers {
                Database.saveInDatabase(answer: i)
            }
            
        } else {
            self.navigationController?.pushViewController(Controller.nextPageVC, animated: true)
        }
    }
    
    func didTapButton(_ tableViewCell: QuizTableViewCell, button: UIButton) {
        guard let indexPath = self.quizTableView.indexPath(for: tableViewCell) else { return }
        
        
        if QuizPageModel.quizModel[QuestionIndex.questionIndex].answers[indexPath.row].answer == button.titleLabel?.text ?? "" {
            for i in 0..<QuizPageModel.quizModel[QuestionIndex.questionIndex].answers.count {
                if QuizPageModel.quizModel[QuestionIndex.questionIndex].answers[i].isSelected == true &&
                   QuizPageModel.quizModel[QuestionIndex.questionIndex].answers[i].answer != QuizPageModel.quizModel[QuestionIndex.questionIndex].answers[indexPath.row].answer {
                        QuizPageModel.quizModel[QuestionIndex.questionIndex].answers[i].isSelected = false
                }
            }
            
            QuizPageModel.quizModel[QuestionIndex.questionIndex].answers[indexPath.row].isSelected.toggle()
        }
        
        quizTableView.reloadData()
    }
}
