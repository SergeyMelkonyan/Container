//
//  QuizPageModel.swift
//  Container
//
//  Created by Sergey Melkonyan on 28.09.22.
//

import Foundation

struct QuizPageModel {
    var question: String
    var image: String
    var answers: [Answer]
}

extension QuizPageModel {
    
    static var quizModel = [
        QuizPageModel(
            question: "Question 1",
            image: "greenImage",
            answers: [
                Answer(answer: "1"),
                Answer(answer: "2"),
                Answer(answer: "3"),
                Answer(answer: "4"),
                Answer(answer: "5"),
                Answer(answer: "6"),
                Answer(answer: "7"),
                Answer(answer: "8"),
                Answer(answer: "9"),
                Answer(answer: "10")
        ]),
        QuizPageModel(
            question: "Question 2",
            image: "secondImage",
            answers: [
                Answer(answer: "11"),
                Answer(answer: "12"),
                Answer(answer: "13"),
                Answer(answer: "14")
        ]),
        QuizPageModel(
            question: "Question 3",
            image: "thirdImage",
            answers: [
                Answer(answer: "15"),
                Answer(answer: "16"),
                Answer(answer: "17"),
                Answer(answer: "18")
        ]),
        QuizPageModel(
            question: "Question 4",
            image: "fourthImage",
            answers: [
                Answer(answer: "19"),
                Answer(answer: "20"),
                Answer(answer: "21"),
                Answer(answer: "22")
        ]),
    ]
}
