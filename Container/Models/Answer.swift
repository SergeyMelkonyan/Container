//
//  Answer.swift
//  Container
//
//  Created by Sergey Melkonyan on 28.09.22.
//

import Foundation
import RealmSwift

struct Answer: Equatable {
    var answer: String
    var isSelected = false
}

class RealmAnswer: Object {
    @objc dynamic var answer: String = ""
    
    override class func primaryKey() -> String? {
        return "answer"
    }
}

extension RealmAnswer {
    func toStruct() -> Answer {
        return Answer(answer: answer)
    }
}

extension Answer {
    func toObject() -> RealmAnswer {
        return RealmAnswer.init()
    }
}
